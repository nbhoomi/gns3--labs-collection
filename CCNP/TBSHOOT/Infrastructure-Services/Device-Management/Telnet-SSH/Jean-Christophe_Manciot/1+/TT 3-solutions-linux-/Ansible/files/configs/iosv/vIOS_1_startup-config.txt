
! Last configuration change at 14:23:59 UTC Mon Sep 5 2016 by admin
!
version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname vIOS_1
!
boot-start-marker
boot-end-marker
!
!
enable secret 5 $1$2z.q$HW88a2JUOVE0OXhjenxYp0
!
aaa new-model
!
!
aaa authentication login default local
aaa authentication login console local
!
!
!
!
!
aaa session-id common
!
!
!
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
!
!
!
!
!
!
!
ip domain name actionmystique.net
ip cef
no ipv6 cef
!
multilink bundle-name authenticated
!
!
cts logging verbose
!
!
username root privilege 15 secret 5 $1$MdLJ$zafu6a8bQfnzLDRcnMagU/
username admin privilege 15 secret 5 $1$tocM$V408zvE84ZcDflbkLmijh/
!
redundancy
!
!
ip ssh time-out 10
ip ssh rsa keypair-name vIOS_1_rsakey
ip ssh version 2
ip ssh dh min size 2048
ip ssh stricthostkeycheck
ip ssh pubkey-chain
  username root
   key-hash ssh-rsa 96208E97B46EC2B2B54895C1C955D51C root@SAMSUNG-Ubuntu
  username admin
   key-hash ssh-rsa 96208E97B46EC2B2B54895C1C955D51C root@SAMSUNG-Ubuntu
ip scp server enable
! 
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 ip address 192.168.137.254 255.255.255.0
 duplex full
 speed auto
 media-type rj45
!
interface GigabitEthernet0/1
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/2
 ip address 10.0.113.1 255.255.255.0
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/3
 ip address 10.0.2.1 255.255.255.0
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/4
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/5
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/6
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/7
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
!
router eigrp veigrp
 !
 address-family ipv4 unicast autonomous-system 65501
  !
  af-interface GigabitEthernet0/0
   passive-interface
  exit-af-interface
  !
  topology base
  exit-af-topology
  network 0.0.0.0
  eigrp router-id 1.1.1.1
 exit-address-family
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
!
line con 0
 exec-timeout 0 0
 logging synchronous
 login authentication console
line aux 0
line vty 0 4
 exec-timeout 0 0
 password 7 112A1016141D595C557C6F
 logging synchronous
 transport input ssh
 transport output ssh
line vty 5 15
 exec-timeout 0 0
 password 7 112A1016141D595C557C6F
 logging synchronous
 transport input ssh
 transport output ssh
!
no scheduler allocate
!
end

