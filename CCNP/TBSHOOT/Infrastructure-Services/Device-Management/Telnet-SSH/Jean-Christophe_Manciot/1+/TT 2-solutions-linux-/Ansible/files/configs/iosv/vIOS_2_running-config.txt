
! Last configuration change at 14:06:32 UTC Mon Sep 5 2016 by admin
!
version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname vIOS_2
!
boot-start-marker
boot-end-marker
!
!
enable secret 5 $1$daZo$uOipk1Y5uoErKycC9fe/x1
!
aaa new-model
!
!
aaa authentication login default local
aaa authentication login console local
!
!
!
!
!
aaa session-id common
!
!
!
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
!
!
!
!
!
!
!
ip domain name actionmystique.net
ip cef
no ipv6 cef
!
multilink bundle-name authenticated
!
!
cts logging verbose
!
!
username root privilege 15 secret 5 $1$6ieW$Yi.RzBGKoJQvzQlx5y0IE.
username admin privilege 15 secret 5 $1$Kl74$f5Sn29tu63GnOAav6ayhT.
!
redundancy
!
!
ip ssh time-out 10
ip ssh rsa keypair-name vIOS_2_rsakey
ip ssh version 2
ip ssh dh min size 2048
ip ssh stricthostkeycheck
ip ssh pubkey-chain
  username root
   key-hash ssh-rsa CCCBC7A1D9982B451E13BCC802FEE661 root@MSI-GE60-Ubuntu
   key-hash ssh-rsa 96208E97B46EC2B2B54895C1C955D51C root@SAMSUNG-Ubuntu
  username admin
   key-hash ssh-rsa 96208E97B46EC2B2B54895C1C955D51C root@SAMSUNG-Ubuntu
ip scp server enable
! 
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/1
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/2
 ip address 10.0.113.2 255.255.255.0
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/3
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/4
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/5
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/6
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/7
 no ip address
 shutdown
 duplex auto
 speed auto
 media-type rj45
!
!
router eigrp veigrp
 !
 address-family ipv4 unicast autonomous-system 65501
  !
  topology base
  exit-af-topology
  network 0.0.0.0
  eigrp router-id 2.2.2.2
 exit-address-family
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv - Cisco Systems Confidential                                      *
*                                                                        *
* This software is provided as is without warranty for internal          *
* development and testing purposes only under the terms of the Cisco     *
* Early Field Trial agreement.  Under no circumstances may this software *
* be used for production purposes or deployed in a production            *
* environment.                                                           *
*                                                                        *
* By using the software, you agree to abide by the terms and conditions  *
* of the Cisco Early Field Trial Agreement as well as the terms and      *
* conditions of the Cisco End User License Agreement at                  *
* http://www.cisco.com/go/eula                                           *
*                                                                        *
* Unauthorized use or distribution of this software is expressly         *
* Prohibited.                                                            *
**************************************************************************^C
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
 login authentication console
line aux 0
line vty 0 4
 exec-timeout 0 0
 logging synchronous
 transport input ssh
 transport output ssh
line vty 5 15
 exec-timeout 0 0
 logging synchronous
 transport input ssh
 transport output ssh
!
no scheduler allocate
!
end

