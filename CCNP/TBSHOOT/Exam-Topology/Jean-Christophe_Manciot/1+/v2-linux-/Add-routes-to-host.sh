#!/bin/bash
ip route add 109.65.200.240/29 via 192.168.137.254 metric 1
ip route add 209.65.200.240/29 via 192.168.137.254 metric 1
ip -6 route add 2002::109:65:200:240/125 via FC00:137::254 metric 1
ip -6 route add 2001::209:65:200:240/125 via FC00:137::254 metric 1
ip route show
ip -6 route show
