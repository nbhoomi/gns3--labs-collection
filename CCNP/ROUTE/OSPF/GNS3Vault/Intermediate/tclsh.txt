tclsh
foreach address {
1.1.1.1
2.2.2.2
3.3.3.3
4.4.4.4
192.168.12.1
192.168.12.2
192.168.13.1
192.168.13.3
192.168.34.3
192.168.34.4
192.168.24.2
192.168.24.4
} {
ping $address }
exit
