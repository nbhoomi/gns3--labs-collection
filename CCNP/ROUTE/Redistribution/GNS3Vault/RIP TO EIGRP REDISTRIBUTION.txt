RIP TO EIGRP REDISTRIBUTION
WRITTEN BY RENE MOLENAAR ON 30 AUGUST 2011. POSTED IN REDISTRIBUTION
SCENARIO
A famous research company is trying to develop a medicine to increase the capabilities of the brain. They are trying to connect their site to another research center to exchange information but there is some trouble with the redistribution of routing protocols. You as a senior network engineer are able to solve this problem without too much trouble..or do you? Let's see!
GOAL:
* All IP addresses have been preconfigured for you as specified in the topology picture.
* Each router has a loopback0 interface.
* Configure EIGRP AS 1 on router Charles, Caesar, Landon and Hunsiker.
* Configure RIP Version 2 on router Rodman, Aranha, Landon and Charles.
* Configured two-way redistribution on router Charles and Landon.
* Achieve full connectivity so you can ping all physical and loopback interfaces.
IOS:
c3640-jk9s-mz.124-16.bin

